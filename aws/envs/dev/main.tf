provider "aws" {
  region = "us-east-1"
}

data "template_file" "install_wordpress" {
  template = file("${path.module}/install-wordpress.sh")
}


locals {
  vm_proxy_user_data = <<-EOT
    sudo apt install -y nginx
    cat <<"EOF" > /tmp/proxy.cfg
      proxy_pass         http://10.0.29.230;
      proxy_set_header   Host $host;
      proxy_set_header   X-Real-IP  $remote_addr;
      proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header   X-Forwarded-Proto https;
      proxy_set_header   X-Forwarded-Host $host;
    EOF
  EOT

  vm_wordpress_user_data = <<-EOT
    cat <<"EOF" > /tmp/install-wordpress.sh
      ${data.template_file.install_wordpress.rendered}
    EOF
    chmod +x /tmp/install-wordpress.sh
    sudo /tmp/install-wordpress.sh
  EOT

  vm_database_user_data = <<-EOT
    sudo docker pull bitnami/mysql
    sudo docker run -d --restart always --name mysql-container \
      -e ALLOW_EMPTY_PASSWORD=yes \
      -e MYSQL_USER=wordpress_user \
      -e MYSQL_PASSWORD=1234 \
      -e MYSQL_DATABASE=wordpress_db \
      -e MYSQL_AUTHENTICATION_PLUGIN=mysql_native_password \
      -p 3306:3306 bitnami/mysql
  EOT
}

module "network" {
  source = "../../network"

  region             = "us-east-1"
  environment        = "dev-web-app"
  availability_zones = ["a", "f"]
}

output "availability_zones" {
  value = module.network.vpc_azs
}

output "subnets" {
  value = module.network.vpc_private_subnets
}

module "vm_bastion" {
  source = "../../vm-bastion"

  region            = "us-east-1"
  environment       = "dev-web-app"
  availability_zone = module.network.vpc_azs["a"]
  vpc_id            = module.network.vpc_id
  vpc_subnet        = module.network.vpc_public_subnets["a"]
}

module "vm_proxy" {
  source = "../../vm-ubuntu"

  region                        = "us-east-1"
  environment                   = "dev-web-app"
  machine_name                  = "proxy"
  machine_type                  = "t2.nano"
  availability_zone             = module.network.vpc_azs["f"]
  vpc_id                        = module.network.vpc_id
  vpc_subnet                    = module.network.vpc_public_subnets["f"]
  associate_public_ip_address   = true
  ingress_cidr_blocks           = ["0.0.0.0/0"]
  ingress_rules                 = ["http-80-tcp"]
  bastion_key                   = module.vm_bastion.key
  bastion_ssh_security_group_id = module.vm_bastion.ssh_security_group_id
  user_data                     = local.vm_proxy_user_data
}

module "vm_wordpress" {
  source = "../../vm-ubuntu"

  region                        = "us-east-1"
  environment                   = "dev-web-app"
  machine_name                  = "wordpress"
  machine_type                  = "t2.nano"
  availability_zone             = module.network.vpc_azs["f"]
  vpc_id                        = module.network.vpc_id
  vpc_subnet                    = module.network.vpc_private_subnets["f"]
  ingress_cidr_blocks           = ["${module.vm_proxy.private_ip}/32"]
  ingress_rules                 = ["http-80-tcp"]
  bastion_key                   = module.vm_bastion.key
  bastion_ssh_security_group_id = module.vm_bastion.ssh_security_group_id
  user_data                     = local.vm_wordpress_user_data
}

module "vm_database" {
  source = "../../vm-docker"

  region                        = "us-east-1"
  environment                   = "dev-web-app"
  machine_name                  = "database"
  machine_type                  = "t2.micro"
  availability_zone             = module.network.vpc_azs["a"]
  vpc_id                        = module.network.vpc_id
  vpc_subnet                    = module.network.vpc_private_subnets["a"]
  ingress_cidr_blocks           = ["${module.vm_wordpress.private_ip}/32"]
  ingress_rules                 = ["mysql-tcp"]
  bastion_key                   = module.vm_bastion.key
  bastion_ssh_security_group_id = module.vm_bastion.ssh_security_group_id
  user_data                     = local.vm_database_user_data
}

