output "vpc_id" {
  value = module.vpc.vpc_id
}

output "vpc_azs" {
  value = zipmap(var.availability_zones, module.vpc.azs)
}

output "vpc_private_subnets" {
  value = zipmap(var.availability_zones, module.vpc.private_subnets)
}

output "vpc_public_subnets" {
  value = zipmap(var.availability_zones, module.vpc.public_subnets)
}
